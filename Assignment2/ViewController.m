//
//  ViewController.m
//  Assignment2
//
//  Created by Jason Clinger on 1/21/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
 
//    UIView* view = [UIView new];
//    view.frame = CGRectZero;
//    view.backgroundColor = [UIColor blueColor];
//    view.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.view addSubview:view];
//    
//    NSDictionary* dictionary = NSDictionaryOfVariableBindings(view);
//    NSDictionary* metrics = @{};
//    
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-100-[view]-100-|" options:0 metrics:metrics views:dictionary]];
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[view(==100)]|" options:0 metrics:metrics views:dictionary]];
    
    label1 = [[UILabel alloc]initWithFrame:CGRectZero];
    label1.text = [NSString stringWithFormat:@"%.f", stepper1.value];
    label1.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:label1];
    
    label2 = [[UILabel alloc]initWithFrame:CGRectZero];
    label2.text = [NSString stringWithFormat:@"%.f", stepper2.value];
    label2.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:label2];
    
    txtfld = [[UITextField alloc]initWithFrame:CGRectZero];
    txtfld.backgroundColor = [UIColor yellowColor];
    txtfld.text = [NSString stringWithFormat:@"Team1"];
    txtfld.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:txtfld];
    
    txtfld2 = [[UITextField alloc]initWithFrame:CGRectZero];
    txtfld2.backgroundColor = [UIColor yellowColor];
    txtfld2.text = [NSString stringWithFormat:@"Team2"];
    txtfld2.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:txtfld2];
    
    btn1 = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn1 setTitle:@"Reset Scores" forState:UIControlStateNormal];
    btn1.frame = CGRectZero;
    btn1.translatesAutoresizingMaskIntoConstraints = NO;
    btn1.backgroundColor =[UIColor blueColor];
    [btn1 addTarget:self action:@selector(btntouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn1];
    
    stepper1 = [[UIStepper alloc]initWithFrame:CGRectZero];
    [stepper1 addTarget:self action:@selector(stepper1Touched:) forControlEvents:UIControlEventValueChanged];
    stepper1.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:stepper1];
    
    stepper2 = [[UIStepper alloc]initWithFrame:CGRectZero];
    [stepper2 addTarget:self action:@selector(stepper2Touched:) forControlEvents:UIControlEventValueChanged];
    stepper2.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:stepper2];
    
    NSDictionary* variableDictionary = NSDictionaryOfVariableBindings(label1, label2, stepper1, stepper2, txtfld, txtfld2, btn1);
    
    NSDictionary* metrics = @{@"margin":@10};
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[txtfld(==100)]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-225-[txtfld2(==100)]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[label1]-150-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-225-[label2]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[stepper1]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-225-[stepper2]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-150-[btn1(==100)]-|" options:0 metrics:metrics views:variableDictionary]];
    
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-margin-[label1(100)]-[txtfld(==50)]-[stepper1]-[btn1]" options:0 metrics:metrics views:variableDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[txtfld(==25)]-[label1(100)]-[stepper1]-[btn1(==50)]-200-|" options:0 metrics:metrics views:variableDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[txtfld2(==25)]-[label2(100)]-[stepper2]-|" options:0 metrics:metrics views:variableDictionary]];
    
}

-(void)btntouched:(UIButton*)btn{
   // self.txtfld.text = @"Touched the button";
    stepper1.value = 0;
    stepper2.value = 0;
    label1.text = [NSString stringWithFormat:@"%.f", stepper1.value];
    label2.text = [NSString stringWithFormat:@"%.f", stepper2.value];}


-(void)stepper1Touched:(UIStepper*)stepper{
    NSLog(@"%.f", stepper.value);
    label1.text = [NSString stringWithFormat:@"%.f", stepper1.value];}

-(void)stepper2Touched:(UIStepper*)stepper{
    NSLog(@"%.f", stepper.value);
    label2.text = [NSString stringWithFormat:@"%.f", stepper2.value];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
